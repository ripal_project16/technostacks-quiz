<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    public $table = 'option';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'points',
        'created_at',
        'updated_at',
        'deleted_at',
        'question_id',
        'option_text',
    ];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
