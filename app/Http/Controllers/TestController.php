<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use DB;
use Mail;


class TestController extends Controller
{
    //

    public function index()
    {
        $questions = Question::with('questionOptions')->get();
        // echo '<pre>'; print_r($questions);die;
        return view('client.index', compact('questions'));
    }

    public function store(Request $request)
    {
        $pointsGain = 0;
        $totalQues  = count($request->questions);
        if( !empty($request->questions) ) {
            foreach ($request->questions as $questions => $options) {
                $isAvailable = DB::table('question_result')->where('option_id', $options)->first();
                if( !empty($isAvailable) ) {
                    $pointsGain++;
                }
            }
        }

        $perc = $this->get_percentage($pointsGain, $totalQues);
        return view('client.results', compact('perc'));
    }

    function get_percentage($pointsGain, $totalQues)
    {
        if ( $pointsGain > 0 ) {
            return ' Congratulations!!!!  Your Percentage: '. round(( 100 * $pointsGain / $totalQues ), 2) . ' %';
        } else {
            return ' Sorry you are not eligible for further round. Try next time.. !!';
        }
    }

    public function send($message) 
    {
        $data = array('name'=>$message);
   
        Mail::send([], $data, function($message) {
            $message->to('ripaldarji16@gmail.com', '')->subject
                ('Laravel Basic Testing Mail');
            $message->from('ripaldarji16@gmail.com','Virat Gandhi');
        });
        die;
        // echo "Basic Email Sent. Check your inbox.";
    }
}
