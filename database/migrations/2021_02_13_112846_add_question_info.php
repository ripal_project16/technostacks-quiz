<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuestionInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Insert some stuff
        DB::table('question')->insert(
            [
                [
                    'question_text' => '56% of Y is 182. What is Y?'
                ],
                [
                    'question_text' => 'Y has to score 40% marks to pass. He gets 20 marks and fails by 40 marks. The maximum marks of the exam are?'
                ],
                [
                    'question_text' => 'Which of the following two ratios is greater 17:18 and 10:11?'
                ],
                [
                    'question_text' => '285 is summation of 3 numbers. Ratio between 2nd and 3rd numbers is 6:5. Ratio between 1st and 2nd numbers is 3:7. The 3rd number is?'
                ],
                [
                    'question_text' => 'A man got Rs. 130 less, as simple interest, when he invested Rs. 2000 for 4 years as compared to investing Rs. 2250 for same duration. What is the rate of interest?'
                ]
            ]
            );

            DB::table('option')->insert(
                [
                    [
                        'option_text' => '364',
                        'question_id' => 1
                    
                    ],
                    [
                        'option_text' => '325' ,
                        'question_id' => 1
                    ],
                    [   
                        'option_text' => '330' ,
                        'question_id' => 1
                    ],
                    [
                        'option_text' => '100' ,
                        'question_id' => 1
                    ],
                    [   
                        'option_text' => '350' ,
                        'question_id' => 2
                    ],
                    [   
                        'option_text' => '200' ,
                        'question_id' => 2],
                    [   
                        'option_text' => '150' ,
                        'question_id' => 2],
                    [   
                        'option_text' => '250' ,
                        'question_id' => 2],
                    [   
                        'option_text' => '17/18',
                        'question_id' => 3],
                    [   
                        'option_text' => '10/11',
                        'question_id' => 3],
                    [   
                        'option_text' => 'Both are same',
                        'question_id' => 3
                    ],
                    [   
                        'option_text' => 'Cannot determine',
                        'question_id' => 3
                    ],
                    [   
                        'option_text' => '135',
                        'question_id' => 4
                    ],
                    [   
                        'option_text' => '150',
                        'question_id' => 4
                    ],
                    [   
                        'option_text' => '124',
                        'question_id' => 4
                    ],
                    [   
                        'option_text' => '105',
                        'question_id' => 4
                    ],
                    [   
                        'option_text' => '12%',
                        'question_id' => 5
                    ],
                    [   
                        'option_text' => '13%',
                        'question_id' => 5
                    ],
                    [   
                        'option_text' => '12.5%',
                        'question_id' => 5
                    ],
                    [   
                        'option_text' => '10.50%',
                        'question_id' => 5
                    ]
    
                    
                ]
            );
    
    
            DB::table('question_result')->insert(
                [
                    [
                        'question_id' => 1,
                        'option_id' => 3
                    ],
                    [
                        'question_id' => 2,
                        'option_id' => 7
                    ],
                    [
                        'question_id' => 3,
                        'option_id' => 9
                    ],
                    [
                        'question_id' => 4,
                        'option_id' => 16
                    ],
                    [
                        'question_id' => 5,
                        'option_id' => 18
                    ]
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
