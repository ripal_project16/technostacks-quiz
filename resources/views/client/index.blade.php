@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Test</div>

                <div class="card-body">
                    @if(session('status'))
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            </div>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('client.startTest') }}" id="quizsubmit">
                        @csrf
                        @foreach($questions as $category)
                                   
                                <div class="card @if(!$loop->last)mb-3 @endif">
                                    <div class="card-header">{{ $category->question_text }}</div>
                
                                    <div class="card-body">
                                        <input type="hidden" name="questions[{{ $category->id }}]" value="">
                                        @foreach($category->questionOptions as $option)
                                            <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="questions[{{ $category->id }}]" id="option-{{ $option->id }}" value="{{ $option->id }}"@if(old("questions.$category->id") == $option->id) checked @endif>
                                                    <label class="form-check-label" for="option-{{ $option->id }}">
                                                        {{ $option->option_text }}
                                                    </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                        @endforeach

                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#quizsubmit').on('submit',function(event){
        event.preventDefault();
        // debugger;
        // let name = $('#name').val();
        // let email = $('#email').val();
        // let mobile_number = $('#mobile_number').val();
        // let subject = $('#subject').val();
        // let message = $('#message').val();

        // $.ajax({
        //   url: "/contact-form",
        //   type:"POST",
        //   data:{
        //     "_token": "{{ csrf_token() }}",
        //     name:name,
        //     email:email,
        //     mobile_number:mobile_number,
        //     subject:subject,
        //     message:message,
        //   },
        //   success:function(response){
        //     console.log(response);
        //   },
        //  });
        });
      </script>
@endsection