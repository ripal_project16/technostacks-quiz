@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Results of your test</div>

                <div class="card-body">
                    @if(session('status'))
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-success" role="alert">
                                    <p>{{ session('status') }}</p>

                                </div>
                            </div>
                        </div>
                    @endif

                    <p>{{ $perc }} </p>

                    {!! Form::open(['method' => 'post', 'route' => 'client.send']) !!}
                    <!-- <a href="{{ route('client.startTest') }}" class="btn btn-primary">Start Test</a> -->
                    <button type="submit" class="btn btn-primary">Email Your Result</button>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection